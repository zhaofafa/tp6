<?php

namespace app\index\controller;

use think\View;
use app\index\model\User;

class ViewTest
{
    // 方法与模板文件的对应
    public function demo1(View $view)
    {
        // 默认的视图目录是view
        return $view->fetch();
    }

    // 模板赋值
    public function demo2(View $view)
    {
        // 变量
        $site = 'PHP中文网';
        $view->assign('sitename', $site);
        // 一组变量
        $view->assign(['name'=>'admin', 'email'=>'admin@php.cn']);

        // 数组
        $data = ['brand'=>'华为', 'model'=>'P20', 'price'=>8888];
        $view->assign('mobile', $data);

        // 对象
        $obj = new \stdClass();
        $obj->course = 'php';
        $obj->grade = 95;

        $view->assign('stu', $obj);

        // 预定义变量: $_GET, $_SERVER

        return $view->fetch();
    }

    // 流程控制
    public function demo3(View $view, User $user)
    {
        $res = $user->db()->select();

        $view->assign('users', $res);

        return $view->fetch();

    }

}
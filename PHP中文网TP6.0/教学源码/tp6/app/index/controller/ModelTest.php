<?php
namespace app\index\controller;

use app\index\model\User;

class ModelTest
{
    // 模型对象
    public function demo1()
    {
        // 创建模型对象
        $user = new User();
        $res = $user->db()->find(1);
        dump($res);
    }

    // 依赖注入
    public function demo2(User $user)
    {
        $res = $user->db()->find(3);
        dump($res);
        var_dump($res);
        echo '<hr>';
        echo $res['name'];
        echo '<br>';
        echo $res->name;
    }

    // 新增操作: create(): 参数就是要新增的数据,返回当前模型对象
    public function insert()
    {
        $data = ['name'=> '乔峰', 'age'=>40, 'email'=>'qiaofeng@php.cn','password'=>sha1('123')];

        $user = User::create($data);

        $insertId = $user['user_id'];

        return '新增成功, 新增记录的主键ID是: ' . $insertId;
    }

    // 查询操作
    // 在tp6中，删除了传统的get/all, 直接用db()来调用Query类中的方法完成
    public function select(User $user)
    {
        // 单条
        $res1 = $user->db()->find(4);
        dump($res1);

        echo '<hr>';
        // all()
        $res2 = $user->db()->where('age','>', 40)->select();
        dump($res2);
    }

    // 更新操作:update
    public function update()
    {
        $user = User::update(['age'=>50], ['user_id'=>2]);
        return '年龄已经被更新成: ' .$user['age'];
    }

    // 删除: destroy(): 返回布尔值
    public function delete()
    {
        $res = User::destroy(function ($query){
            $query->where('user_id', 4);
        });
        return $res ? '删除成功': '删除失败';
    }




}
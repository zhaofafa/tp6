<?php


namespace app\index\controller;

use think\facade\Db;

class DbTest
{
    // 原生查询的读操作: query()
    public function demo1()
    {
        $sql = "SELECT `user_id`, `name`,`age` FROM `user` WHERE `age`>:age LIMIT :num";
        $map = ['age'=> 25, 'num'=>2];
        $res = Db::query($sql, $map);
        dump($res);
    }

    // 原生查询中的写操作: insert/update/delete, execute()
    public function demo2()
    {
        $sql = "UPDATE `user` SET `age`=:age WHERE `user_id`=:user_id ";
        $map = ['age'=> 32, 'user_id'=>2];
        $res = Db::execute($sql, $map);
        return '成功更新了 ' . $res . ' 条记录';
    }

    // 查询构造器
    // table(): 设置数据表
    // field(): 设置查询字段列表
    // find(): 返回满足条件的第一条记录,单条记录
    public function demo3()
    {
        $res = Db::table('user')
            ->field('user_id, name, age')
            ->find(2); // 支持将主键做为参数
        dump($res);
    }

    // select(): 返回满足条件的多条记录
    public function demo4()
    {
        $res = Db::table('user')
            ->field('user_id, name, age')
            ->select([2,3,4]); // 支持将主键做为参数
        dump($res);
    }

    // where(): 设置查询条件 , 字符串, 表达式, 数组
    // fetchSql(): true, false
    public function demo5()
    {
        $res = Db::table('user')
            ->field('user_id, name, age')
            // 1. 字符串
//            ->where('user_id = 1')
//            ->where('age > 30')
                // 2. 表达式: (字段, 操作符， 值)
            ->where('user_id', 2)
            ->where('age',  '>', 30)
            // 区间，模糊查询
//            ->where('age',  'between', [20, 30])
                // 3 数组
                // 关联数组: 等值查询,AND
//            ->where(['user_id'=>2, 'age'=>30])
            // 索引数组: 批量查询
            ->where([
               0=> ['age',  'between', [20, 30]],
//                1=>['age',  '>', 30]
            ])
            ->fetchSql(false)
            ->select();
        dump($res);
    }

    // order(), limit()
    public function demo6()
    {
        $res = Db::table('user')
            ->field('user_id, name, age')
//            ->order('age desc')  // asc 升序， desc 降序
//            ->order('age', 'desc')
            // 多字段排序，用数组参数
            ->order(['user_id'=>'asc', 'age'=> 'desc'])
            ->limit(2,2)
            ->select();
        dump($res);

    }
}
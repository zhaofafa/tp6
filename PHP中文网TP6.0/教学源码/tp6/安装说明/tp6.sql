-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `tp6` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;
USE `tp6`;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` tinyint(3) unsigned NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` char(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user` (`user_id`, `name`, `age`, `email`, `password`) VALUES
(1,	'韦小宝',	25,	'weixiaobao@php.cn',	'e10adc3949ba59abbe56e057f20f883e'),
(2,	'小龙女',	50,	'xiaolongnv@php.cn',	'e10adc3949ba59abbe56e057f20f883e'),
(3,	'过儿',	19,	'guoer@php.cn',	'e10adc3949ba59abbe56e057f20f883e'),
(6,	'欧阳克',	27,	'oyangke@php.cn',	'e10adc3949ba59abbe56e057f20f883e');

-- 2019-05-28 05:13:48
# 安装说明

## 1. 环境要求

* PHP 7.1+
* Windows / Linux:
  * phpStudy / php 工具箱
 * MacOS: MAMP Pro 5+
 
 ## 2. 安装步骤
 
 * 创建本地虚拟主机与主机名,如: `tp6.io`
 * 并将主机名解析到项目的`public`目录下
 * 使用数据库管理工具, 如phpMyAdmin / Adminer
 * 将本目录下的`tp6.sql`文件导入
 
 ## 3. 访问
 
 * 浏览器访问, 如`tp6.io`， 不报错则安装成功
 
 > 更多课程，请关注: <www.php.cn>
 